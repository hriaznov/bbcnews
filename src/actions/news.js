import axios from 'axios';
import * as types from '../actions/types';

export function getNews () {
	return dispatch => {
		dispatch(getNewsRequest());

		axios.get(`https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Ffeeds.bbci.co.uk%2Fnews%2Frss.xml`).then(response => {
			const newsData = response.data.items;

			dispatch(getNewsSuccess(newsData));
		}).catch((error) => {
			dispatch(getNewsFailed(error));
		});
	};
}

export function getNewsRequest () {
	return {
		type: types.GET_NEWS_REQUEST
	}
}

export function getNewsSuccess (newsData) {
	return {
		type: types.GET_NEWS_SUCCESS,
		newsData
	}
}

export function getNewsFailed (error) {
	console.error(error);

	return {
		type: types.GET_NEWS_FAILED
	}
}