import * as types from '../actions/types';

const news = (state = {}, action) => {
	switch (action.type) {
		case types.GET_NEWS_REQUEST:
			return state;

		case types.GET_NEWS_SUCCESS:
			return Object.assign({}, state, action.newsData);

		case types.GET_NEWS_FAILED:
			return state;

		default:
			return state;
	}
};

export default news;