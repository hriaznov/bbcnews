import React from 'react';
import { AnimatedSwitch } from 'react-router-transition';
import {Route} from 'react-router-dom';
import NotFound from '../NotFound';
import NewsContent from "../NewsContent";

export default () => {
	return (
		<AnimatedSwitch
			atEnter={{ opacity: 0, left: 50 }}
			atLeave={{ opacity: 0, left: -50 }}
			atActive={{ opacity: 1, left: 0 }}
			className="switch-wrapper"
		>
			<Route exact path="/" component={NewsContent} />
			<Route component={NotFound} />
		</AnimatedSwitch>
	);
};