import React from 'react';
import BaseLayout from './BaseLayout';

const NotFound = () => {
	return (
		<BaseLayout>
			<h1>404 - Page not found</h1>
		</BaseLayout>
	);
};

export default NotFound;