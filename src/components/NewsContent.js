import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../actions/news';
import BaseLayout from "./BaseLayout";
import OverlayItem from './OverlayItem';
import LinkBox from './LinkBox';

class NewsContent extends Component {
	componentWillMount() {
		this.props.actions.getNews();
	}

	newsAnimationInit = () => {
		const importedScript = document.createElement('script');
		importedScript.src = './vendors/demo.js';
		document.body.appendChild(importedScript);
	};

	render() {
		const { newsData } = this.props;
		let linksArr = [];
		let overlayItems = [];

		if (Object.keys(newsData).length) {
			for (let i in newsData) {
				const splittedTitle = newsData[i].title.split(' ');

				linksArr = [
					...linksArr,
					<LinkBox
						key={+i + 1}
						number={+i + 1}
						plate={{text: splittedTitle[0], position: (+i + 1) % 2 === 0 ? 'bottom' : 'top'}}
						content={newsData[i].title}
						title={{text: splittedTitle[1], position: (+i + 1) % 2 === 0 ? 'horizontal' : 'vertical'}}
					/>
				];

				overlayItems = [
					...overlayItems,
					<OverlayItem
						key={+i + 1}
						number={+i + 1}
						plate={{text: splittedTitle[0], position: (+i + 1) % 2 === 0 ? 'bottom' : 'top'}}
						content={newsData[i].description}
						title={{text: splittedTitle[1], position: (+i + 1) % 2 === 0 ? 'horizontal' : 'vertical'}}
					/>
				];
			}
		}

		this.newsAnimationInit();

		return (
			<BaseLayout>
				<main className="NewsContent">
					<div className="content">
						<div className="grid">
							{linksArr}
						</div>
					</div>
					<div className="overlay">
						<div className="overlay__reveal" />
						{overlayItems}
						<button className="overlay__close">x</button>
					</div>
				</main>
			</BaseLayout>
		);
	}
}

function mapStateToProps(state) {
	return {
		newsData: state.news
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(actions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsContent);