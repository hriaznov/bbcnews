import React from 'react';

const OverlayItem = (props) => {
	return (
		<div className="overlay__item" id={`preview-${props.number}`}>
			<div className="box">
				<div className="box__shadow" />
				<img className="box__img" src={`/media/original/${props.number}.jpg`} alt="Some pic"/>
				<h3 className={`box__title ${props.title.position === 'horizontal' ? 'box__title--straight box__title--left' : ''}`}><span className="box__title-inner" data-hover={props.title.text}>{props.title.text}</span></h3>
				<h4 className={`box__text ${props.plate.position === 'bottom' ? 'box__text--bottom' : ''}`}><span className="box__text-inner">{props.plate.text}</span></h4>
				<div className="box__deco">{props.number}</div>
				{props.title.position === 'horizontal' ? <div className="box__deco box__deco--top">➩</div> : null}
			</div>
			<p className="overlay__content">{props.content}</p>
		</div>
	);
};

export default OverlayItem;