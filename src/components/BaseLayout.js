import React from 'react';
import { Link } from 'react-router-dom';

const BaseLayout = (props) => {
	return (
		<div className="App">
			<header className="App-header">
				<Link to="/welcome"><h1 className="App-title">BBC News</h1></Link>
			</header>

			<div className="App-wrapper">
				{props.children}
			</div>
		</div>
	);
};

export default BaseLayout;