import React from 'react';
import ReactDOM from 'react-dom';
import './style/vendors/base.css';
import './style/style.css';
import App from './App';
import { store } from './store/configureStore';
import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);

registerServiceWorker();
